require('dotenv').config();
const express = require('express');
const { handleErrorResponse } = require('./_helpers/programmerErrorHandler');
const cors = require('cors');
const  app = express();
const cookieSession = require('cookie-session');
app.use(cookieSession({
    keys: ['secret2020', 'secret2020']
}));
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cors());
const port = process.env.APP_ENV === 'production' ? (process.env.PORT || 80) : 2020;
app.use(function(req, res, next) {
  var allowedOrigins = process.env.ALLOWED_ORIGINS;
  var origin = req.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization, userid"
  );
  res.header("Access-Control-Allow-Credentials", true);
  next();
});
app.use(cors());
app.use('/announcement', require('./controllers/announcement'));
app.use('/content', require('./controllers/content'));
app.use('/user', require('./controllers/user'));
app.use('/page', require('./controllers/page'));
app.use('/question', require('./controllers/question'));
app.use('/selectedusers', require('./controllers/selectedUsers'));
app.use('/center', require('./controllers/center'));
//This middleware will run once no used route was found 
app.all('*', (req, res, next) => {
  res.status(404).json({
    status: 'fail',
    message: `Can't find ${req.method} ${req.originalUrl} on this server!`
  });
});

//Error middleware for programmer errors or bugs
app.use((err, req, res, next) => {
  /* If you call next() with an error after you have started writing the response 
  * (for example, if you encounter an error while streaming the response to the client) 
  * the Express default error handler closes the connection and fails the request.
  * So when you add a custom error handler, you must delegate to the default Express error handler,
  *  when the headers have already been sent to the client: */
  if (res.headersSent) {
    return next(err)
  }
  handleErrorResponse(err, res);
});

//report errors that are generated globally, operational errors
//app.use(require('./_helpers/operationalErrorHandler'));
//open port connection
app.listen(port, () => {
    console.log(`YDT API Application running on port ${port}`);
});