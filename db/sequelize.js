const Sequelize = require('sequelize');
const UserModel = require('../models/user');
const RenewalModel = require('../models/renewal');
const AnnModel = require("../models/announcement");
const QuestionModel = require("../models/question");
const CenterModel = require("../models/center");
const FedbackModel = require("../models/feedback");
const ViewProfileModel = require("../models/viewprofile");
const PageModel = require('../models/page');
const SelectedUsersModel = require('../models/selectedUsers');
//declare and set values for evironment constants
const Db_name =
  process.env.APP_ENV === "production" ? process.env.DB_NAME : "ydt";
const Db_user =
  process.env.APP_ENV === "production" ? process.env.DB_USER : "root";
const Db_pass =
  process.env.APP_ENV === "production" ? process.env.DB_PASS : "root";
const Host =
  process.env.APP_ENV === "production" ? process.env.DB_HOST : "localhost";
const sequelize = new Sequelize(Db_name, Db_user, Db_pass, {
  host: Host,
  port: 3306,
  dialect: "mysql",
  pool: {
    max: 1, //in production this could be 5. Reduced to one for Heroku
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  //global settings for models definition
  define: {
    underscored: true,
    timestamps: true,
    freezeTableName: true, // Model tableName will be the same as the model name
    createdAt: "created_at",
    // I want updatedAt to actually be called updateTimestamp
    updatedAt: "updated_at",
    deletedAt: "deleted_at"
  },
  // alter database table it doesn't correspond sequelise schema definition
  sync: {
    alter: process.env.APP_ENV === "production" ? false : true,
    force: process.env.APP_ENV === "production" ? false : true,
  },
});

// test connection
sequelize
  .authenticate()
  .then(() => {
    console.log("Sequelize connection has been established successfully.");
  })
  .catch((err) => {
    console.error("Sequelize is unable to connect to the database:", err);
  })
// Connect all the models/tables in the database to a db object,
// so everything is accessible via one object
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.User = UserModel(sequelize, Sequelize)
db.Renewal = RenewalModel(sequelize,Sequelize);
db.Announcement = AnnModel(sequelize,Sequelize);
db.Question = QuestionModel(sequelize,Sequelize);
db.Center = CenterModel(sequelize,Sequelize);
db.Feedback = FedbackModel(sequelize,Sequelize);
db.ViewProfile = ViewProfileModel(sequelize,Sequelize);
db.Page = PageModel(sequelize,Sequelize);
db.SelectedUsers = SelectedUsersModel(sequelize,Sequelize);

//Model/tables associations
db.User.hasMany(db.Renewal, {foreignKey : 'user_id'});
db.Center.hasMany(db.User, {foreignKey: 'center_id'});
db.User.belongsTo(db.Center);
db.Center.hasMany(db.Feedback,{foreignKey: 'center_id'});
db.Feedback.belongsTo(db.Center);
db.Center.hasMany(db.Page, {foreignKey: 'center_id'});
db.User.hasMany(db.ViewProfile, {foreignKey:'user_id'});
db.ViewProfile.belongsTo(db.User);
db.User.hasMany(db.Renewal,{foreignKey: 'user_id'});
db.Renewal.belongsTo(db.User);
db.Center.hasMany(db.Announcement, {foreignKey:'center_id'});
db.Announcement.belongsTo(db.Center);
db.Center.hasMany(db.Question, {foreignKey:'center_id'});
db.Question.belongsTo(db.Center);

module.exports = { db };