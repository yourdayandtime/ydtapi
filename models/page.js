'use strict'
const Page = (sequelize, DataTypes) => {
  return sequelize.define(
    "page",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      title: DataTypes.STRING(100),
      text: DataTypes.STRING(1000),
      ref: DataTypes.STRING(1000),
      user_id: DataTypes.INTEGER(11),
      center_id: {type: DataTypes.INTEGER(11), defaultValue: 1},
      is_published: { type: DataTypes.INTEGER(1), defaultValue: 1 },//not publishing automatically 
      ref: DataTypes.STRING(200),
      type: DataTypes.STRING(20),
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = Page;