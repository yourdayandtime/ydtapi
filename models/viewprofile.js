'use strict'
const ViewProfile = (sequelize, DataTypes) => {
  return sequelize.define(
    "view_profile",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      user_id: DataTypes.INTEGER(11),
      sec_code: DataTypes.STRING(6),
      sec_code_ok: DataTypes.INTEGER(1),
      updated_fields: { type: DataTypes.JSON, defaultValue: null }, 
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = ViewProfile;