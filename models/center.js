'use strict'
const Center = (sequelize, DataTypes) => {
  return sequelize.define(
    "center",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      name: DataTypes.STRING(100),
      text: DataTypes.STRING,
      province: DataTypes.STRING(100),
      is_published: { type: DataTypes.INTEGER(1), defaultValue: 1 },//not publishing automatically 
      color_cls: DataTypes.STRING(20),
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = Center;