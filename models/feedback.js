'use strict'
const Feedback = (sequelize, DataTypes) => {
  return sequelize.define(
    "feedback",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      text: DataTypes.STRING(1000),
      name: DataTypes.STRING(50),
      user_id: DataTypes.INTEGER(11),
      is_published: { type: DataTypes.INTEGER(1), defaultValue: 1 },//not publishing automatically 
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = Feedback;