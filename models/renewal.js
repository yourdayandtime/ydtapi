'use strict'
const Renewal = (sequelize, DataTypes) => {
  return sequelize.define(
    "renewal",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      user_id: DataTypes.INTEGER,
      center_id: DataTypes.INTEGER(11),
      exp_date: DataTypes.DATE,
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
    },
    {
      deleted_at: false,
    }
  );
};
module.exports = Renewal;
