const { db } = require("../../db/sequelize");
const Op = db.Sequelize.Op;
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
async function create(data) {
  return await db.Announcement.create(data);
}
async function update(annId, announcementParam) {
  var announcement = await db.Announcement.findOne({
    where: { id: annId },
  });
  if (!announcement) throw new ErrorHandler(404, "announcement not found");
  Object.assign(announcement, announcementParam);
  if ((announcement = await announcement.save())) {
    return { status: "success", announcement: announcement };
  }
}

async function findByField(field, val) {
  console.log(field + " " + val);
  return await db.Announcement.findOne({
    where: { [field]: val },
    include: [
      {
        model: db.Renewal,
        order: [["created_at", "DESC"]],
      },
    ],
  });
}

async function getLatest() {
  return await db.Announcement.findAll({
    limit: 1,
    where: {
      is_published: 1,
      [Op.or]: [
        { center_id: { [Op.eq]: 1 } },
        { center_id: { [Op.eq]: null } },//show on all centers
      ],
    },
    order: [["updated_at", "DESC"]],
  });
}
async function getAll() {
  return await db.Announcement.findAll({
    where: {
      // is_published: 1,
      [Op.or]: [
        { center_id: { [Op.eq]: 1 } },
        { center_id: { [Op.eq]: null } },//show on all centers
      ],
    },
    order: [["id", "DESC"]],
  });
}
async function userGetAll() {
  return await db.Announcement.findAll({
    limit: 20,
    where: {
      is_published: 1,
      [Op.or]: [
        { center_id: { [Op.eq]: 1 } },
        { center_id: { [Op.eq]: null } },//show on all centers
      ],
    },
    order: [["id", "DESC"]],
  });
}
async function _delete(id) {
  let deleted = await db.Announcement.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "Announcement couldn't be deleted");
  return deleted;
}
async function publish(id) {
   return this.update(id,{is_published:1});
}
module.exports = {
  create,
  update,
  getLatest,
  getAll,
  userGetAll,
  findByField,
  _delete,
  publish
};
