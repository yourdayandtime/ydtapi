const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
const Op = db.Sequelize.Op;
async function create(data) {
  return await db.SelectedUsers.create(data);
}

async function save(data) {
  if (typeof data === "object") {
    if (data.hasOwnProperty("id")) {
      var id = data.id;
      delete data.id;
      return await update(id, data);
    } else {
      return await create(data);
    }
  }
}
async function getListTitles() {
  let selectedUsers = await db.SelectedUsers.findAndCountAll({
    offset: 0,
    limit: 20,
    attributes: ["id", "title"],
    where: { center_id: 1 },
  });
  return selectedUsers;
}
async function update(id, selectedUsersParam) {
  var selectedUsers = await db.SelectedUsers.findOne({
    where: { id: id },
  });
  if (!selectedUsers) throw new ErrorHandler(404, "SelectedUsers not found");
  Object.assign(selectedUsers, selectedUsersParam);
  if ((selectedUsers = await selectedUsers.save())) {
    return { status: "success", selectedUsers: selectedUsers };
  }
}
async function getLatest() {
  let selectedList = await db.SelectedUsers.findOne({
    where: { center_id: 1 },
     order: [["created_at", "DESC"]],
  });
  var data = {};
  if(selectedList) {
    data = await getSelectedUsers(selectedList.user_ids);
      Object.assign(data, {
        id: selectedList.id,
        title: selectedList.title,
        reason: selectedList.reason,
      });
  }
  return data;
}
async function findByField(field, val) {
  let selectedList = await db.SelectedUsers.findOne({
    where:{ [field]: val }
  });
  var data = {};
  if(selectedList) {
    data = await getSelectedUsers(selectedList.user_ids);
      Object.assign(data, {
        id: selectedList.id,
        title: selectedList.title,
        reason: selectedList.reason,
      });
  }
  return data;
}
async function getSelectedUsers(ids) {
  var data = await db.User.findAndCountAll({
    offset: 0,
    limit: 20,
    where: {
      id: { [Op.in]: JSON.parse(ids) },
    },
  });
  return data;
}
async function adminFindOne(id) {
let selectedList = await db.SelectedUsers.findOne({
    where: { center_id: 1 },
  });
  var data = {};
  if(selectedList) {
    data = await getSelectedUsers(selectedList.user_ids);
      Object.assign(data, {
        id: selectedList.id,
        title: selectedList.title,
        reason: selectedList.reason,
      });
  }
  return data;
}
async function adminGetAll() {
  let selectedUsers = await db.SelectedUsers.findAndCountAll({
    offset: 0,
    limit: 20,
    where: { center_id: 1 },
  });
  let users = [];
  if (selectedUsers.count) {
    var rows = selectedUsers.rows;
    for (var i = 0; i < selectedUsers.count; i++) {
      console.log(rows[i].user_ids);
      var data = await getSelectedUsers(rows[i].user_ids);
      Object.assign(data, {
        id: rows[i].id,
        title: rows[i].title,
        reason: rows[i].reason,
      });
      users.push(data);
    }
  }
  return users;
}
async function _delete(id) {
  let deleted = await db.SelectedUsers.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "SelectedUsers couldn't be deleted");
  return deleted;
}

module.exports = {
  create,
  update,
  getLatest,
  adminGetAll,
  findByField,
  _delete,
  getSelectedUsers,
  save,
  getListTitles,
  adminFindOne,
};
