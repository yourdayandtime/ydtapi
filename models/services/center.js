const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
async function create(data) {
  return await db.Center.create(data);
}
async function update(id, CenterParam) {
  var Center = await db.Center.findOne({
    where: { id: id },
  });
  if (!Center) throw new ErrorHandler(404, "Center not found");
  Object.assign(Center, CenterParam);
  if ((Center = await Center.save())) {
    return { status: "success", Center: Center };
  }
}

async function findByField(field, val) {
  console.log(field + " " + val);
  return await db.Center.findOne({
    where: { [field]: val },
  });
}
async function getAll() {
  return await db.Center.findAll({
    where: {is_published: 1},
    order: [["name", "ASC"]],
  });
}
async function _delete(id) {
  let deleted = await db.Center.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "Center couldn't be deleted");
  return deleted;
}

module.exports = {
  create,
  update, 
  getAll,
  findByField,
  _delete,
};
