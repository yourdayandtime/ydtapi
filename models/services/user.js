const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
async function create(data) {
  return await db.User.create(data);
}
async function update(userId, userParam) {
  var user = await db.User.findOne({
    where: { id: userId },
  });
  if (!user) throw new ErrorHandler(404, "User not found");
  Object.assign(user, userParam);
  if ((user = await user.save())) {
    return { status: "success", user: user };
  }
}
async function isAvailable(field, value) {
  var val = await db.User.findOne({
    where: { [field]: value },
  });
  if (val) return false;
  else return true;
}
async function findByField(field, val) {
  console.log(field + " " + val);
  return await db.User.findOne({
    where: { [field]: val },
    include: [
      {
        model: db.Renewal,
        order: [["created_at", "DESC"]],
      },
    ],
  });
}

async function getAll() {
  return await db.User.findAll({
    /* offset: 0,
    limit: 20, */
    //DataTable plugin does its own paginations
    order: [["id", "DESC"]],
    include: [
      {
        model: db.Renewal,
        order: [["created_at", "DESC"]],
      },
    ],
  });
}

async function _delete(id) {
  let deleted = await db.User.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "User couldn't be deleted");
  return deleted;
}

async function getRefugeeProfile(obj) {
  var attrs = [
    "id",
    "fname",
    "lname",
    "gender",
    "genderString",
    "birth_year",
    "permit_type",
    "permit_no",
    "exp_date",
    "refugee_center",
    "country_iso",
    'country',
    "phone",
    "email",
  ];
  if (obj.hasOwnProperty("phone"))
    return await db.User.findOne({
      attributes: attrs,
      where: { phone: obj.phone },
    });
  else if (obj.hasOwnProperty("permit_no"))
    return await db.User.findOne({
      attributes: attrs,
      where: { permit_no: obj.permit_no },
    });
  return null;
}

module.exports = {
  create,
  update,
  getAll,
  findByField,
  _delete,
  isAvailable,
  getRefugeeProfile,
};
