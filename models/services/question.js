const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
const Op = db.Sequelize.Op;
async function save(data) {
  if(data.hasOwnProperty('id') && parseInt(data.id) > 0) {
    var id = parseInt(data.id);
    return await this.update(id, data);
  } else {
    return await this.create(data);
  }
}
async function create(data) {
  return await db.Question.create(data);
}
async function update(annId, questionParam) {
  var question = await db.Question.findOne({
    where: { id: annId },
  });
  if (!question) throw new ErrorHandler(404, "question not found");
  Object.assign(question, questionParam);
  if ((question = await question.save())) {
    return { status: "success", question: question };
  }
}

async function findByField(field, val) {
  console.log(field + " " + val);
  return await db.Question.findOne({
    where: { [field]: val },
  });
}
async function getLatest() {
  return await db.Question.findAll({
    limit: 1,
    where: {
      is_published: 1,
      [Op.or]: [
        { center_id: { [Op.eq]: 1 } },
        { center_id: { [Op.eq]: null } }, //show on all centers
      ],
    },
    order: [["created_at", "DESC"]],
  });
}
async function getAll() {
  return await db.Question.findAndCountAll({
    offset: 0,
    limit: 20,
    where: {
      is_published: 1,
    },
    order: [["id", "DESC"]],
  });
}

async function adminGetAll() {
  return await db.Question.findAndCountAll({
    offset: 0,
    limit: 20,
    order: [["id", "DESC"]],
  });
}

async function _delete(id) {
  let deleted = await db.Question.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "question couldn't be deleted");
  return deleted;
}

module.exports = {
  create,
  save,
  update,
  getLatest,
  adminGetAll,
  getAll,
  findByField,
  _delete,
};
