const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
//const Op = db.Sequelize.Op;
async function create(data) {
  return await db.ViewProfile.create(data);
}
async function update(id, ViewProfileParam) {
  const ViewProfile = await db.ViewProfile.findOne({
    where: { id: id },
  });
  if (!ViewProfile) throw new ErrorHandler(404, "ViewProfile not found");
  Object.assign(ViewProfile, ViewProfileParam);
  if ((ViewProfile = await ViewProfile.save())) {
    return { status: "success", ViewProfile: ViewProfile };
  }
}

async function getAll() {
  return await db.ViewProfile.findAndCountAll({
    offset:0,
    limit: 10,
    where: {center_id:1},//center id do brought in dynamically
    order: [["updated_at", "DESC"]],
  });
}
async function _delete(id) {
  let deleted = await db.ViewProfile.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "ViewProfile couldn't be deleted");
  return deleted;
}

module.exports = {
  create,
  update,
  getAll,
  _delete,
};