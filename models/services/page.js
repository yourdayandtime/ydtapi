const { db } = require("../../db/sequelize");
const { ErrorHandler } = require("../../_helpers/programmerErrorHandler");
const stringSanitizer = require("string-sanitizer");
//const Op = db.Sequelize.Op;
async function create(data) {
  var ref = stringSanitizer.sanitize.addDash(data.title);
  var page = await db.Page.create(data);
  if(page) {
    Object.assign(page,{ref:ref.toLowerCase() + '-' + page.id});//making it unique
    return await page.save();
  }
  return page;
}
async function update(id, PageParam) {
  var Page = await db.Page.findOne({
    where: { id: id },
  });
  if (!Page) throw new ErrorHandler(404, "Page not found");
  Object.assign(Page, PageParam);
  if ((Page = await Page.save())) {
    return { status: "success", Page: Page };
  }
}
async function getLatest() {
  var p = await db.Page.findOne({
    order: [["created_at", "DESC"]]
  });
  return p;
}
async function findByField(field, val) {
  var p = await db.Page.findOne({
    where: { [field]: val },
  });
  return p;
}
async function getMenus(centerId){
  return await db.Page.findAll({
    attributes:['id','title','type','ref'],
    limit: 10,
    where: { is_published: 1, center_id: centerId },
    order: [["updated_at", "DESC"]],
  })
}
async function getAll() {
  return await db.Page.findAndCountAll({
    offset: 0,
    limit: 10,
    where: { is_published: 1, center_id: 1 }, //center id do brought in dynamically
    order: [["updated_at", "DESC"]],
  });
}
async function _delete(id) {
  let deleted = await db.Page.destroy({ where: { id: id }, limit: 1 });
  if (!deleted) throw ErrorHandler(500, "Page couldn't be deleted");
  return deleted;
}

module.exports = {
  create,
  getLatest,
  update,
  findByField,
  getAll,
  _delete,
  getMenus
};
