module.exports.users = [
    {
      fname: "Leanne Graham",
      lname: "Bret",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      fname: "Ervin Howell",
      lname: "Antonette",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      fname: "Clementine Bauch",
      lname: "Samantha",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Patricia Lebsack",
      lname: "Karianne",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Chelsey Dietrich",
      lname: "Kamren",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Mrs. Dennis Schulist",
      lname: "Leopoldo_Corkery",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Kurtis Weissnat",
      lname: "Elwyn.Skiles",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Nicholas Runolfsdottir V",
      lname: "Maxime_Nienow",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Glenna Reichert",
      lname: "Delphine",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
    {
      name: "Clementina DuBuque",
      lname: "Moriah.Stanton",
      gender: gender(),
      phone: phoneNumber(),
      country_iso: iso(),
      permit_no: permitNo(),
      permit_type: permitType(),
      exp_date: randomDate(),
      ts_cs: 1,
    },
  ];
function phoneNumber() {
  //var no = Math.floor(100000000 + Math.random() * 900000000);
  var saNo = [7, 8, 6];
  return "+27" + saNo[Math.floor(Math.random() * saNo.length)] + Math.random().toString().slice(2, 11);
}

function permitNo() {
  var centers = ["CTR", "DBN", "PTR", "JHB", "PTB", "MSN"];
  return (
    centers[Math.floor(Math.random() * centers.length)] +
    "/" +
    Math.random().toString().slice(2, 8) +
    "/" +
    Math.random().toString().slice(2, 4)
  );
}

function permitType() {
  var types = ["Status", "Asylum", "Appeal"];
  return types[Math.floor(Math.random() * types.length)];
}

function countries() {
  var c = [
    { AO: "Angola" },
    { BI: "Burundi" },
    { CM: "Cameroon" },
    { CG: "Congo-Brazaville" },
    { CD: "Congo-Kinshasa" },
    { CI: "Ivory Cost" },
    { ET: "Ethopia" },
    { GN: "Guinea" },
    { GW: "Guinea-Bissau" },
    { KE: "Kenya" },
    { MD: "Madagascar" },
    { MW: "Malawi" },
    { ML: "Mali" },
    { NA: "Namibia" },
    { NE: "Niger" },
    { NG: "Nigeria" },
    { PK: "Pakistan" },
    { RW: "Rwanda" },
    { SN: "Senegal" },
    { SL: "Sierra Leone" },
    { SO: "Somalia" },
    { SD: "Sudan" },
    { TZ: "Tanzania" },
    { TG: "Togo" },
    { UG: "Uganda" },
    { ZM: "Zambia" },
    { ZW: "Zimbabwe" },
  ];
  return c[Math.floor(Math.random() * c.length)];
}

function iso() {
  var c = [
    "AO",
    "BI",
    "CM",
    "CG",
    "CD",
    "CI",
    "ET",
    "GN",
    "GW",
    "KE",
    "MD",
    "MW",
    "ML",
    "NA",
    "NE",
    "NG",
    "PK",
    "RW",
    "SN",
    "SL",
    "SO",
    "SD",
    "TZ",
    "TG",
    "UG",
    "ZM",
    "ZW",
  ];
  return c[Math.floor(Math.random() * c.length)];
}

function gender() {
  var g = ["m", "f"];
  return g[Math.floor(Math.random() * g.length)];
}

function randomDate() {
    var start = new Date(2020, 0, 1);
    var end = new Date();
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}
