'use strict'
const Question = (sequelize, DataTypes) => {
  return sequelize.define(
    "question",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      asked_by: DataTypes.INTEGER(11),
      question: DataTypes.STRING(500),
      answer: DataTypes.STRING,
      answered_by: DataTypes.INTEGER(11),
      center_id: DataTypes.INTEGER(11),
      is_published: { type: DataTypes.INTEGER(1), defaultValue: 0 },//not publishing automatically 
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = Question;
