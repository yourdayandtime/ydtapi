"use strict";
const User = (sequelize, DataTypes) => {
  return sequelize.define(
    "user",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      fname: DataTypes.STRING(100),
      lname: DataTypes.STRING(100),
      fullName: {
        type: DataTypes.VIRTUAL,
        get() {
          return this.getDataValue("fname") + " " + this.getDataValue("lname");
        },
      },
      dob: DataTypes.DATE,
      gender: DataTypes.TEXT("tiny"),
      genderString: {
        type: DataTypes.VIRTUAL,
        get() {
          if (!this.getDataValue("gender")) {
            return "unknown";
          }
          return this.getDataValue("gender") === "f" ? "Female" : "Male";
        },
      },
      birth_year: DataTypes.STRING(10),
      email: { type: DataTypes.STRING(50), unique: true },
      email_validated: DataTypes.INTEGER(1),
      phone: DataTypes.STRING(20),
      phone_validated: DataTypes.INTEGER(1),
      password: DataTypes.STRING(255),
      country_iso: DataTypes.STRING(4),
      country: {
        type: DataTypes.VIRTUAL,
        get() {
          if (!countries[this.getDataValue("country_iso")]) {
            return "unknown";
          }
          return countries[this.getDataValue("country_iso")];
        },
      },
      region: {
        type: DataTypes.VIRTUAL,
        get() {
          var countries = require('../countries.json');
          if (sadec[this.getDataValue("country_iso")]) {
            return "Sadc";
          } else {
            for(var i = 0; i < countries.length; i++) {
              if(countries[i]['Two_Letter_Country_Code'] === this.getDataValue('country_iso')) {
                if (countries[i]['Continent_Name'] === 'Asia') {
                  return countries[i]['Continent_Name'];
                }
                if (countries[i]['Continent_Name'] === 'Africa' && !sadec[this.getDataValue("country_iso")]) {
                  return 'North Africa';
                }
                return countries[i]['Continent_Name'];
              }
            }
          }
          return ''
        },
      },
      province: DataTypes.STRING(100),
      city: DataTypes.STRING(100),
      center_id: DataTypes.INTEGER(11),
      refugee_center: DataTypes.STRING(50),
      permit_no: { type: DataTypes.STRING(50), defaultValue: null },
      permit_type: { type: DataTypes.STRING(50), defaultValue: "asylum" },
      exp_date: DataTypes.DATE,
      exp_status: {
        type: DataTypes.VIRTUAL,
        get() {
          //exp_day return a value in the folowing format '2000-12-31'
          var exp_day = new Date(this.getDataValue("exp_date"));
          var d = new Date();
          var weekDay = d.getDay();
          //make today with same format as exp_day
          var today = new Date(
            d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            0,
            0,
            0
          );
          //console.log(today);
          const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
          const substracted = exp_day - today;
          if (substracted > 0) {
            var daysLeft = Math.round(Math.abs((exp_day - today) / oneDay));
          } else {
            var daysLeft = -1;
          }
          if (daysLeft === 1) {
            if (isWeekend(weekDay)) return "next week";
            else return "tomorrow";
          } else if (daysLeft === 0) {
            if (isWeekend(weekDay)) return "next week";
            else return "today";
          } else if (daysLeft < 0) {
            return "expired";
          } else if (daysLeft <= 5) {
            if (isWeekend(weekDay)) return "next week";
            else return "this week";
          } else if (daysLeft < 10) {
            if (isWeekend(weekDay)) return "next week";
            else return "today";
          } else if (daysLeft <= 21) return "this month";
          if (daysLeft > 21) return "later";
          return "unknown";
        },
      },
      renew_date: DataTypes.DATE,
      is_hao: { type: DataTypes.INTEGER(1), defaultValue: 0 }, //is home affairs official or not
      is_active: { type: DataTypes.INTEGER(1), defaultValue: 0 }, //user should be approved within 24 hours after signup
      ts_cs: { type: DataTypes.INTEGER(1), defaultValue: 1 },
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: { type: DataTypes.DATE },
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
function isWeekend(d) {
  if (d === 6 || d === 0) return true;
  else return false;
}
var sadec = {
  AO: "Angola",
  BW: "Botswana",
  KM: "Comoros",
  CD: "Congo - Kinshasa",
  LS: "Lesotho",
  MG: "Madagascar",
  MW: "Malawi",
  MU: "Mauritius",
  MZ: "Mozambique",
  NA: "Namibia",
  SC: "Seychelles",
  TZ: "Tanzania",
  ZM: "Zambia",
  ZW: "Zimbabwe",
};
var asia = {CN: "China"};
var countries = {
  DZ: "Algeria",
  AO: "Angola",
  BJ: "Benin",
  BW: "Botswana",
  BF: "Burkina Faso",
  BI: "Burundi",
  CM: "Cameroon",
  CV: "Cape Verde",
  CF: "Central African Republic",
  TD: "Chad",
  KM: "Comoros",
  CG: "Congo - Brazzaville",
  CD: "Congo - Kinshasa",
  CI: "Côte d’Ivoire",
  DJ: "Djibouti",
  EG: "Egypt",
  GQ: "Equatorial Guinea",
  ER: "Eritrea",
  ET: "Ethiopia",
  GA: "Gabon",
  GM: "Gambia",
  GH: "Ghana",
  GN: "Guinea",
  GW: "Guinea-Bissau",
  KE: "Kenya",
  LS: "Lesotho",
  LR: "Liberia",
  LY: "Libya",
  MG: "Madagascar",
  MW: "Malawi",
  ML: "Mali",
  MR: "Mauritania",
  MU: "Mauritius",
  YT: "Mayotte",
  MA: "Morocco",
  MZ: "Mozambique",
  NA: "Namibia",
  NE: "Niger",
  NG: "Nigeria",
  RW: "Rwanda",
  RE: "Réunion",
  SH: "Saint Helena",
  SN: "Senegal",
  SC: "Seychelles",
  SL: "Sierra Leone",
  SO: "Somalia",
  ZA: "South Africa",
  SD: "Sudan",
  SZ: "Swaziland",
  ST: "São Tomé and Príncipe",
  TZ: "Tanzania",
  TG: "Togo",
  TN: "Tunisia",
  EH: "Western Sahara",
  AI: "Anguilla",
  AG: "Antigua and Barbuda",
  AR: "Argentina",
  AW: "Aruba",
  BS: "Bahamas",
  BB: "Barbados",
  BZ: "Belize",
  BM: "Bermuda",
  BO: "Bolivia",
  BR: "Brazil",
  VG: "British Virgin Islands",
  CA: "Canada",
  KY: "Cayman Islands",
  CL: "Chile",
  CO: "Colombia",
  CR: "Costa Rica",
  CU: "Cuba",
  DM: "Dominica",
  DO: "Dominican Republic",
  EC: "Ecuador",
  SV: "El Salvador",
  FK: "Falkland Islands",
  GF: "French Guiana",
  GL: "Greenland",
  GD: "Grenada",
  GP: "Guadeloupe",
  GT: "Guatemala",
  GY: "Guyana",
  HT: "Haiti",
  HN: "Honduras",
  JM: "Jamaica",
  MQ: "Martinique",
  MX: "Mexico",
  MS: "Montserrat",
  AN: "Netherlands Antilles",
  NI: "Nicaragua",
  PA: "Panama",
  PY: "Paraguay",
  PE: "Peru",
  PR: "Puerto Rico",
  BL: "Saint Barthélemy",
  KN: "Saint Kitts and Nevis",
  LC: "Saint Lucia",
  MF: "Saint Martin",
  PM: "Saint Pierre and Miquelon",
  VC: "Saint Vincent and the Grenadines",
  SR: "Suriname",
  TT: "Trinidad and Tobago",
  TC: "Turks and Caicos Islands",
  VI: "U.S. Virgin Islands",
  US: "United States",
  UY: "Uruguay",
  VE: "Venezuela",
  AF: "Afghanistan",
  AM: "Armenia",
  AZ: "Azerbaijan",
  BH: "Bahrain",
  BD: "Bangladesh",
  BT: "Bhutan",
  BN: "Brunei",
  KH: "Cambodia",
  CN: "China",
  CY: "Cyprus",
  GE: "Georgia",
  HK: "Hong Kong SAR China",
  IN: "India",
  ID: "Indonesia",
  IR: "Iran",
  IQ: "Iraq",
  IL: "Israel",
  JP: "Japan",
  JO: "Jordan",
  KZ: "Kazakhstan",
  KW: "Kuwait",
  KG: "Kyrgyzstan",
  LA: "Laos",
  LB: "Lebanon",
  MO: "Macau SAR China",
  MY: "Malaysia",
  MV: "Maldives",
  MN: "Mongolia",
  MM: "Myanmar [Burma]",
  NP: "Nepal",
  NT: "Neutral Zone",
  KP: "North Korea",
  OM: "Oman",
  PK: "Pakistan",
  PS: "Palestinian Territories",
  YD: "People's Democratic Republic of Yemen",
  PH: "Philippines",
  QA: "Qatar",
  SA: "Saudi Arabia",
  SG: "Singapore",
  KR: "South Korea",
  LK: "Sri Lanka",
  SY: "Syria",
  TW: "Taiwan",
  TJ: "Tajikistan",
  TH: "Thailand",
  TL: "Timor-Leste",
  TR: "Turkey",
  TM: "Turkmenistan",
  AE: "United Arab Emirates",
  UZ: "Uzbekistan",
  VN: "Vietnam",
  YE: "Yemen",
  AL: "Albania",
  AD: "Andorra",
  AT: "Austria",
  BY: "Belarus",
  BE: "Belgium",
  BA: "Bosnia and Herzegovina",
  BG: "Bulgaria",
  HR: "Croatia",
  CY: "Cyprus",
  CZ: "Czech Republic",
  DK: "Denmark",
  DD: "East Germany",
  EE: "Estonia",
  FO: "Faroe Islands",
  FI: "Finland",
  FR: "France",
  DE: "Germany",
  GI: "Gibraltar",
  GR: "Greece",
  GG: "Guernsey",
  HU: "Hungary",
  IS: "Iceland",
  IE: "Ireland",
  IM: "Isle of Man",
  IT: "Italy",
  JE: "Jersey",
  LV: "Latvia",
  LI: "Liechtenstein",
  LT: "Lithuania",
  LU: "Luxembourg",
  MK: "Macedonia",
  MT: "Malta",
  FX: "Metropolitan France",
  MD: "Moldova",
  MC: "Monaco",
  ME: "Montenegro",
  NL: "Netherlands",
  NO: "Norway",
  PL: "Poland",
  PT: "Portugal",
  RO: "Romania",
  RU: "Russia",
  SM: "San Marino",
  RS: "Serbia",
  CS: "Serbia and Montenegro",
  SK: "Slovakia",
  SI: "Slovenia",
  ES: "Spain",
  SJ: "Svalbard and Jan Mayen",
  SE: "Sweden",
  CH: "Switzerland",
  UA: "Ukraine",
  SU: "Union of Soviet Socialist Republics",
  GB: "United Kingdom",
  VA: "Vatican City",
  AX: "Åland Islands",
  AS: "American Samoa",
  AQ: "Antarctica",
  AU: "Australia",
  BV: "Bouvet Island",
  IO: "British Indian Ocean Territory",
  CX: "Christmas Island",
  CC: "Cocos [Keeling] Islands",
  CK: "Cook Islands",
  FJ: "Fiji",
  PF: "French Polynesia",
  TF: "French Southern Territories",
  GU: "Guam",
  HM: "Heard Island and McDonald Islands",
  KI: "Kiribati",
  MH: "Marshall Islands",
  FM: "Micronesia",
  NR: "Nauru",
  NC: "New Caledonia",
  NZ: "New Zealand",
  NU: "Niue",
  NF: "Norfolk Island",
  MP: "Northern Mariana Islands",
  PW: "Palau",
  PG: "Papua New Guinea",
  PN: "Pitcairn Islands",
  WS: "Samoa",
  SB: "Solomon Islands",
  GS: "South Georgia and the South Sandwich Islands",
  TK: "Tokelau",
  TO: "Tonga",
  TV: "Tuvalu",
  UM: "U.S. Minor Outlying Islands",
  VU: "Vanuatu",
  WF: "Wallis and Futuna",
  ZM: "Zambia",
  ZW: "Zimbabwe",
};

module.exports = User;
