'use strict'
const Announcement = (sequelize, DataTypes) => {
  return sequelize.define(
    "announcement",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      user_id: DataTypes.INTEGER(11),
      title: DataTypes.STRING(100),
      text: DataTypes.STRING,
      center_id: DataTypes.INTEGER(11),
      is_published: { type: DataTypes.INTEGER(1), defaultValue: 0 },//not publishing automatically 
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};
  
module.exports = Announcement;
