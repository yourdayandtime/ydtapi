"use strict";
const SelectedUsers = (sequelize, DataTypes) => {
  return sequelize.define(
    "selected_users",
    {
      id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
      user_ids: DataTypes.STRING,
      ids: {
        type: DataTypes.VIRTUAL,
        get() {
          if (this.getDataValue("user_ids")) {
            var ids = this.getDataValue('user_ids');
            ids = JSON.parse(ids);
            return ids;
          } else {
            return [];
          }
        },
      },
      title: DataTypes.STRING(100),
      reason: DataTypes.STRING(1000),
      is_published: DataTypes.INTEGER(1),
      created_at: { type: DataTypes.DATE, defaultValue: DataTypes.NOW },
      updated_at: { type: DataTypes.DATE },
      end_at: DataTypes.DATE,
      deleted_at: DataTypes.DATE,
    },
    {
      paranoid: true,
    }
  );
};

module.exports = SelectedUsers;
