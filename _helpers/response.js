module.exports = responseHelper;

function responseHelper(statusText, msg) {
    let text = (typeof statusText === 'string'  ? statusText : 'failed');
    let msgText = (msg && typeof msg === 'string' ? msg : '');
    return {status: text, message: msgText};
}