-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 10, 2020 at 10:17 PM
-- Server version: 5.7.30-0ubuntu0.18.04.1
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ydt`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `content` text COLLATE latin1_general_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `center_id` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `content`, `user_id`, `center_id`, `is_published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Address: \r\nCustom House, Old government building, corner Heerenchat & Aderly Street.\r\nCape Town\r\nWestern Cape\r\n8000\r\nSouth Africa\r\nEmail: \r\nakos.essel@dha.gov.za\r\nPhone: \r\n021 421 9173 / 9200\r\nFax: \r\n086 670 4434\r\nMobile: \r\n066 300 7281\r\nhttp://www.dha.gov.za', 1, NULL, 1, '2020-06-05 19:04:25', '2020-06-05 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `center`
--

CREATE TABLE `center` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `text` text COLLATE latin1_general_ci,
  `province` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `center`
--

INSERT INTO `center` (`id`, `name`, `text`, `province`, `is_published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cape Town', '<p>Centre Manager:  Ms Akos Essel</p>\r\n<p>Email: akos.essel@dha.gov.za</p>\r\n<p>Phone: 021 421 9173 / 9200</p>\r\n<p>Mobile: 066 300 7281</p>\r\n<p>Fax: 086 670 4434</p>\r\n<p>Address: Custom House, Old government building, corner Heerenchat & Aderly Street. 8000</p>', 'Western Cape', 1, '2020-06-06 21:21:45', '2020-06-10 02:22:53', NULL),
(2, 'Johannesburg', NULL, 'Gauteng', 1, '2020-06-06 21:22:15', '2020-06-09 17:19:05', '2020-06-09 00:00:00'),
(3, 'Durban', '<p>Centre Manager: Ms Naleen Balgobind</p>\r\n<p>naleen.balgobind@dha.gov.za</p>\r\n<p>Phone: 031362 1201</p>\r\n<p>Fax: 031 362 1220</p>\r\n<p>Kwazulu-Natal, South Africa</p>', 'Kwazulu-Natal', 1, '2020-06-06 21:22:49', '2020-06-09 17:19:31', NULL),
(4, 'Pretoria', '<p>Desmond Tutu Refugee Reception Center </p><p>Centre Manager: Ms Bangwalang Chiloane</p>\r\n<p>Email: bangwalang.chiloane@dha.gov.za</p>\r\n<p>Phone: 012 395 4174 / 4000</p>\r\n<p>Mobile: 066 473 0631</p>\r\n<p>Fax: 012 327 5782</p>\r\n<p>Gauteng, South Africa</p>', 'Gauteng', 1, '2020-06-06 21:23:29', '2020-06-09 17:17:36', NULL),
(5, 'Port Elizabeth', '<p>Refugee Reception Centre (closed to new applications)</p>\r\n<p>Centre Manager: Mr Sabelo Ngxitho</p>\r\n<p>Email: sabelo.ngxitho@dha.gov.za</p>\r\n<p> Eastern Cape, South Africa</p>', 'Eastern Cape', 1, '2020-06-06 21:23:57', '2020-06-09 17:16:27', NULL),
(6, 'Musina', '<p>Centre Manager: Mr Jimmy Malemela</p>\r\n<p>Email: jimmy.malemela@dha.gov.za</p>\r\n<p>Phone: 015 534 5300</p>\r\n<p>Fax: 083 852 0104</p>\r\n<p>Limpopo, South Africa</p>', 'Mpumalanga', 1, '2020-06-06 21:25:33', '2020-06-09 17:11:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `text` text COLLATE latin1_general_ci,
  `name` varchar(50) COLLATE latin1_general_ci DEFAULT 'unknown',
  `user_id` int(11) DEFAULT NULL,
  `center_id` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `goodtoknow`
--

CREATE TABLE `goodtoknow` (
  `id` int(11) NOT NULL,
  `text` text COLLATE latin1_general_ci,
  `user_id` int(11) DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `asked_by` varchar(100) DEFAULT NULL,
  `question` varchar(500) DEFAULT NULL,
  `answer` text,
  `answered_by` int(11) DEFAULT NULL,
  `center_id` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `asked_by`, `question`, `answer`, `answered_by`, `center_id`, `is_published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(83, 'Tresor', 'When do I renew my permit during COVID-19?', 'During COVID-19 times, no Refugee reception center is open. We will observe the progression f the situation and you will be notified in due course.', NULL, 0, 1, '2020-06-05 21:49:32', '2020-06-09 09:36:13', NULL),
(84, 'Precius', 'I lost my asylum seeker permit, how do i get a new one?', 'Just wait a bit...We\'ll update this answer soon.', NULL, 0, 1, '2020-06-05 22:20:28', '2020-06-09 09:36:17', NULL),
(85, 'Peter', 'What to bring in order to join family', 'Here is what you need to bring:...', NULL, 0, 1, '2020-06-05 22:21:36', '2020-06-09 09:36:22', NULL),
(86, 'Ira', 'How do i know when to collect my refugee ID document ?', 'Your ID will be collected whenever you come.', NULL, 0, 1, '2020-06-05 22:23:56', '2020-06-08 19:37:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `renewal`
--

CREATE TABLE `renewal` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `center_id` int(11) NOT NULL,
  `exp_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fname` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `lname` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `birth_year` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `gender` tinytext COLLATE latin1_general_ci,
  `phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `phone_validated` int(1) DEFAULT '0',
  `email` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `email_validated` int(1) DEFAULT '0',
  `password` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `is_hao` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `country_iso` varchar(4) COLLATE latin1_general_ci DEFAULT 'ZA',
  `province` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `city` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `center_id` int(11) DEFAULT NULL,
  `permit_no` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `permit_type` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `exp_date` date DEFAULT NULL,
  `renew_date` date DEFAULT NULL,
  `ts_cs` tinyint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fname`, `lname`, `dob`, `birth_year`, `gender`, `phone`, `phone_validated`, `email`, `email_validated`, `password`, `is_hao`, `is_active`, `country_iso`, `province`, `city`, `center_id`, `permit_no`, `permit_type`, `exp_date`, `renew_date`, `ts_cs`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hannah ', 'Mali', NULL, '2000-2020', 'f', '0718048298', 0, 'hannah211@forcovid-app.com', 0, 'test1234', 0, 0, 'ZA', NULL, NULL, NULL, 'DBR/003488/06', 'asylum', '2019-12-01', NULL, 1, '2020-05-22 18:08:33', '2020-05-22 18:08:33', NULL),
(2, 'Precious', 'Nyarambi', NULL, '1981-1990', 'f', '0718048290', 0, 'hannah2020@forcovid-app.com', 0, 'test1234', 0, 0, 'ZA', NULL, NULL, NULL, 'PTA/902299/20', 'asylum', '2021-01-07', NULL, 1, '2020-05-22 18:25:04', '2020-05-22 18:25:04', NULL),
(3, 'Trésor', 'Mwema', NULL, '1971-1980', 'm', '0718048298', 0, 'tresor747@gmail.com', 0, NULL, 0, 0, 'CD', NULL, NULL, NULL, 'CTR/009531/06', 'status', '2024-03-21', NULL, 1, '2020-06-03 08:13:41', '2020-06-03 08:13:41', NULL),
(4, 'Leanne Graham', 'Bret', NULL, NULL, 'm', '+278946839557', 0, NULL, 0, NULL, 0, 0, 'NA', NULL, NULL, NULL, 'PTB/760380/85', 'Appeal', '2020-04-13', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(5, 'Ervin Howell', 'Antonette', NULL, NULL, 'f', '+278957708332', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'MSN/216528/06', 'Appeal', '2020-01-26', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(6, 'Clementine Bauch', 'Samantha', NULL, NULL, 'm', '+277307932879', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/730440/57', 'Appeal', '2020-02-13', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(7, 'Karianne', 'Karianne', NULL, NULL, 'f', '+277800265579', 0, NULL, 0, NULL, 0, 0, 'ZW', NULL, NULL, NULL, 'MSN/090189/38', 'Appeal', '2020-03-28', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(8, 'Kamren', 'Kamren', NULL, NULL, 'm', '+278912979710', 0, NULL, 0, NULL, 0, 0, 'CI', NULL, NULL, NULL, 'PTR/216887/22', 'Asylum', '2020-01-23', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(9, 'Corkery', 'Valan', NULL, NULL, 'm', '+277448307876', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/105225/22', 'Appeal', '2020-05-28', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(10, 'Elwyns', 'Elwyn.Skiles', NULL, NULL, 'f', '+278134038035', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'MSN/562118/22', 'Status', '2020-02-23', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(11, 'Brook', 'Asta', NULL, NULL, 'm', '+276826481340', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'JHB/690222/80', 'Status', '2020-01-14', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(12, 'Delphine', 'Delphine', NULL, NULL, 'f', '+278716067708', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'JHB/680152/98', 'Appeal', '2020-04-08', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(13, 'Moriah.Stanton', 'Moriah.Stanton', NULL, NULL, 'f', '+278344706319', 0, NULL, 0, NULL, 0, 0, 'GW', NULL, NULL, NULL, 'PTR/924571/39', 'Appeal', '2020-05-07', NULL, 1, '2020-06-03 20:45:16', '2020-06-03 20:45:16', NULL),
(14, 'Leanne Graham', 'Bret', NULL, NULL, 'm', '+278946839557', 0, NULL, 0, NULL, 0, 0, 'NA', NULL, NULL, NULL, 'PTB/760380/85', 'Appeal', '2020-04-13', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(15, 'Ervin Howell', 'Antonette', NULL, NULL, 'f', '+278957708332', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'MSN/216528/06', 'Appeal', '2020-01-26', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(16, 'Clementine Bauch', 'Samantha', NULL, NULL, 'm', '+277307932879', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/730440/57', 'Appeal', '2020-02-13', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(17, 'Sylvain', 'Karianne', NULL, NULL, 'f', '+277800265579', 0, NULL, 0, NULL, 0, 0, 'ZW', NULL, NULL, NULL, 'MSN/090189/38', 'Appeal', '2020-03-28', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(18, 'Isula', 'Manda', NULL, NULL, 'm', '+278912979710', 0, NULL, 0, NULL, 0, 0, 'CI', NULL, NULL, NULL, 'PTR/216887/22', 'Asylum', '2020-01-23', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(19, 'Pangala', 'Cody', NULL, NULL, 'm', '+277448307876', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/105225/22', 'Appeal', '2020-05-28', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(20, 'Nicole', 'Elvis', NULL, NULL, 'f', '+278134038035', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'MSN/562118/22', 'Status', '2020-02-23', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(21, 'Kitwe', 'Nienow', NULL, NULL, 'm', '+276826481340', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'JHB/690222/80', 'Status', '2020-01-14', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(22, 'Alphonse', 'Delphine', NULL, NULL, 'f', '+278716067708', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'JHB/680152/98', 'Appeal', '2020-04-08', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(23, 'Karibu', 'Lisa', NULL, NULL, 'f', '+278344706319', 0, NULL, 0, NULL, 0, 0, 'GW', NULL, NULL, NULL, 'PTR/924571/39', 'Appeal', '2020-05-07', NULL, 1, '2020-06-03 20:49:56', '2020-06-03 20:49:56', NULL),
(24, 'Leanne Graham', 'Bret', NULL, NULL, 'm', '+278946839557', 0, NULL, 0, NULL, 0, 0, 'NA', NULL, NULL, NULL, 'PTB/760380/85', 'Appeal', '2020-04-13', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(25, 'Ervin Howell', 'Antonette', NULL, NULL, 'f', '+278957708332', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'MSN/216528/06', 'Appeal', '2020-01-26', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(26, 'Clementine Bauch', 'Samantha', NULL, NULL, 'm', '+277307932879', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/730440/57', 'Appeal', '2020-02-13', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(27, 'Jambo', 'Karianne', NULL, NULL, 'f', '+277800265579', 0, NULL, 0, NULL, 0, 0, 'ZW', NULL, NULL, NULL, 'MSN/090189/38', 'Appeal', '2020-03-28', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(28, 'Sami', 'Wilson', NULL, NULL, 'm', '+278912979710', 0, NULL, 0, NULL, 0, 0, 'CI', NULL, NULL, NULL, 'PTR/216887/22', 'Asylum', '2020-01-23', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(29, 'Kafubu', 'Kitoko', NULL, NULL, 'm', '+277448307876', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/105225/22', 'Appeal', '2020-05-28', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(30, 'Sylvie', 'Konde', NULL, NULL, 'f', '+278134038035', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'MSN/562118/22', 'Status', '2020-02-23', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(31, 'Wilo', 'MATATA', NULL, NULL, 'm', '+276826481340', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'JHB/690222/80', 'Status', '2020-01-14', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(32, 'Mado', 'Moke', NULL, NULL, 'f', '+278716067708', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'JHB/680152/98', 'Appeal', '2020-04-08', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(33, 'Sandy', 'Moriah', NULL, NULL, 'f', '+278344706319', 0, NULL, 0, NULL, 0, 0, 'GW', NULL, NULL, NULL, 'PTR/924571/39', 'Appeal', '2020-05-07', NULL, 1, '2020-06-03 20:55:15', '2020-06-03 20:55:15', NULL),
(34, 'Leanne Graham', 'Bret', NULL, NULL, 'm', '+278946839557', 0, NULL, 0, NULL, 0, 0, 'NA', NULL, NULL, NULL, 'PTB/760380/85', 'Appeal', '2020-04-13', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(35, 'Ervin Howell', 'Antonette', NULL, NULL, 'f', '+278957708332', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'MSN/216528/06', 'Appeal', '2020-01-26', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(36, 'Clementine Bauch', 'Samantha', NULL, NULL, 'm', '+277307932879', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/730440/57', 'Appeal', '2020-02-13', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(37, 'Lemon', 'Karianne', NULL, NULL, 'f', '+277800265579', 0, NULL, 0, NULL, 0, 0, 'ZW', NULL, NULL, NULL, 'MSN/090189/38', 'Appeal', '2020-03-28', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(38, 'Okongo', 'Kamren', NULL, NULL, 'm', '+278912979710', 0, NULL, 0, NULL, 0, 0, 'CI', NULL, NULL, NULL, 'PTR/216887/22', 'Asylum', '2020-01-23', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(39, 'Samuel', 'Leopoldo', NULL, NULL, 'm', '+277448307876', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'PTB/105225/22', 'Appeal', '2020-05-28', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(40, 'Fofo', 'Skiles', NULL, NULL, 'f', '+278134038035', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'MSN/562118/22', 'Status', '2020-02-23', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(41, 'Tumba', 'Eloy', NULL, NULL, 'm', '+276826481340', 0, NULL, 0, NULL, 0, 0, 'ZM', NULL, NULL, NULL, 'JHB/690222/80', 'Status', '2020-01-14', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(42, 'Inga', 'Kubeka', NULL, NULL, 'f', '+278716067708', 0, NULL, 0, NULL, 0, 0, 'CM', NULL, NULL, NULL, 'JHB/680152/98', 'Appeal', '2020-04-08', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(43, 'Mimi', 'Senga', NULL, NULL, 'f', '+278344706319', 0, NULL, 0, NULL, 0, 0, 'GW', NULL, NULL, NULL, 'PTR/924571/39', 'Appeal', '2020-05-07', NULL, 1, '2020-06-03 20:55:27', '2020-06-03 20:55:27', NULL),
(44, 'Nkwembe', 'Mwema', NULL, '1960-1970', 'm', '0718048298', 0, 'tresor747@gmail.com', 0, NULL, 0, 0, 'ZA', NULL, NULL, NULL, 'ct/009535/06', 'asylum', '2019-12-10', NULL, 1, '2020-06-10 13:44:45', '2020-06-10 13:44:45', NULL),
(45, 'Nkwembe', 'Mwema', NULL, '1960-1970', 'm', '0718048298', 0, 'tresor747@gmail.com', 0, NULL, 0, 0, 'ZA', NULL, NULL, NULL, 'ct/009535/06', 'asylum', '2019-12-10', NULL, 1, '2020-06-10 13:45:07', '2020-06-10 13:45:07', NULL),
(46, 'Tom', 'Mabasha', NULL, '1960-1970', 'm', '718048298', 0, 'tresor747@gmail.com', 0, NULL, 0, 0, 'ZA', NULL, NULL, NULL, 'ct/009535/0', 'asylum', '2019-09-10', NULL, 1, '2020-06-10 13:49:06', '2020-06-10 13:49:06', NULL),
(47, 'Nkwembe', 'Mwema', NULL, '1971-1980', 'm', '0712314412', 0, '', 0, NULL, 0, 0, 'CD', NULL, NULL, NULL, 'CTR/009531/07', 'status', '2020-12-03', NULL, 1, '2020-06-10 19:15:45', '2020-06-10 19:15:45', NULL),
(48, 'Albert', 'Soap', NULL, '1950-1960', 'm', '0814562784', 0, '', 0, NULL, 0, 0, 'CD', NULL, NULL, NULL, 'CTR/009531/04', 'status', '2020-11-20', NULL, 1, '2020-06-10 19:24:09', '2020-06-10 19:24:09', NULL),
(49, 'Nodemon', 'Npm', NULL, '1950-1960', 'f', '0614948345', 0, '', 0, NULL, 0, 0, 'CI', NULL, NULL, NULL, 'CTRCOD0953106', 'asylum', '2021-01-15', NULL, 1, '2020-06-10 19:42:26', '2020-06-10 19:42:26', NULL),
(50, 'Stephane', 'Twazi', NULL, '1950-1960', 'm', '0715555555', 0, '', 0, NULL, 0, 0, 'NA', NULL, NULL, NULL, '00953506QQQQQQQ', 'status', '2020-06-25', NULL, 1, '2020-06-10 19:45:21', '2020-06-10 19:45:21', NULL),
(51, 'Histrian', 'Odgo', NULL, '1971-1980', 'm', '0818989824', 0, '', 0, NULL, 0, 0, 'SN', NULL, NULL, NULL, 'SNG79792759779', 'asylum', '2020-12-05', NULL, 1, '2020-06-10 19:55:53', '2020-06-10 19:55:53', NULL),
(52, 'Victorias', 'Pistorious', NULL, '1971-1980', 'm', '0729450023', 0, '', 0, NULL, 0, 0, 'ZA', NULL, NULL, NULL, 'PTRA00953506', 'status', '2024-02-21', NULL, 1, '2020-06-10 20:04:32', '2020-06-10 20:04:32', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `center`
--
ALTER TABLE `center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goodtoknow`
--
ALTER TABLE `goodtoknow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `renewal`
--
ALTER TABLE `renewal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `center`
--
ALTER TABLE `center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `goodtoknow`
--
ALTER TABLE `goodtoknow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `renewal`
--
ALTER TABLE `renewal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
