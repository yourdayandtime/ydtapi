const express = require("express");
const router = express.Router();
const AnnSvc = require("../models/services/announcement");
const QuestionSvc = require("../models/services/question");
const FeedbackSvc = require("../models/services/feedback");
const CenterSvc = require("../models/services/center");
router.get("/announcement", getLatestAnn);
router.get("/announcements", getAllAnns);
router.get("/question", getLatestQuestion);
router.get("/questions", getAllQuestions);
router.get("/feedback", getLatestFeedback);
router.get("/questions", getAllFeedbacks);
router.get("/centers", getAllCenters);
//export module, every file in node is a module
module.exports = router;

//define announcements middlewares
function getAllAnns(req, res, next) {
  AnnSvc.userGetAll()
    .then((anns) => res.json(anns))
    .catch((err) => next(err));
}
function getLatestAnn(req, res, next) {
  AnnSvc.getLatest()
    .then((ann) => res.json(ann))
    .catch((error) => next(error));
}
//define questions middlewares
function getAllQuestions(req, res, next) {
  QuestionSvc.getAll()
    .then((questions) => res.json(questions))
    .catch((err) => next(err));
}
function getLatestQuestion(req, res, next) {
  QuestionSvc.getLatest()
    .then((question) => res.json(question))
    .catch((error) => next(error));
}
//define feedback middlewares
function getAllFeedbacks(req, res, next) {
  FeedbackSvc.getAll()
    .then((feedbacks) => res.json(feedbacks))
    .catch((err) => next(err));
}
function getLatestFeedback(req, res, next) {
  FeedbackSvc.getLatest()
    .then((feedback) => res.json(feedback))
    .catch((error) => next(error));
}
//refugee Centers accross the country
//define feedback middlewares
function getAllCenters(req, res, next) {
  CenterSvc.getAll()
    .then((centers) => res.json(centers))
    .catch((err) => next(err));
}
