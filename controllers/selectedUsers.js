const express = require("express");
const router = express.Router();
const selectedUsersService = require("../models/services/selectedUsers");
router.post("/create", create);
router.get('/latest',latest);
router.put("/update", update);
router.delete("/delete", _delete);
router.get("/getAll", getAll);
router.get("/findByField/:field/:value", findByField);
router.get("/getMenu", getMenu);
//export module, every file in node is a module
module.exports = router;
function getMenu() {
  selectedUsersService.getListTitles()
    .then((selecteds) => res.json(selecteds))
    .catch((error) => next(error));
}
function getAll(req, res, next) {
  selectedUsersService.adminGetAll()
    .then((selecteds) => res.json(selecteds))
    .catch((error) => next(error));
}

function findByField(req, res, next) {
  selectedUsersService.findByField(req.params.field, req.params.value)
    .then((selecteds) => res.json(selecteds))
    .catch((error) => next(error));
}
function create(req, res, next) {
  selectedUsersService.save(req.body)
    .then((selecteds) => res.json(selecteds))
    .catch((error) => next(error));
}
function latest(req, res, next) {
  selectedUsersService.getLatest()
    .then((selecteds) => res.json(selecteds))
    .catch((error) => next(error));
}
function update(req, res, next) {
  selectedUsersService.save(req.body)
    .then((selecteds) => (selecteds ? res.json(selecteds) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function _delete(req, res, next) {
  selectedUsersService._delete(req.body.id)
    .then((del) => res.json(del))
    .catch((err) => next(err));
}