const express = require("express");
const router = express.Router();
const userService = require("../models/services/user");
router.get("/", getAll);
router.post("/signup", register);
router.get("/current", getCurrent);
router.put("/update", update);
router.delete("delete", _delete);
router.get("/findbyfield/:fieldname/:fieldval", findByField);
router.post("/isavailable", checkAvailability);
router.post("/getProfile", getRefugeeProfile);

//export module, every file in node is a module
module.exports = router;

function checkAvailability(req, res, next) {
  var field = req.body.field;
  var val = req.body.value;
  userService
    .isAvailable(field, val)
    .then((resp) => res.json(resp))
    .catch((err) => next(err));
}

//define middlewares
function getAll(req, res, next) {
  userService
    .getAll()
    .then((users) => res.json(users))
    .catch((err) => next(err));
}
function register(req, res, next) {
  userService
    .create(req.body)
    .then((user) => res.json(user))
    .catch((error) => next(error));
}
function getCurrent(req, res, next) {
  userService
    .getById(req.userid)
    .then((user) => (user ? res.json(user) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function findByField(req, res, next) {
  console.log(req.params);
  userService
    .findByField(req.params.fieldname, req.params.fieldval)
    .then((user) => (user ? res.json(user) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function update(req, res, next) {
  userService
    .update(req.body.id,req.body)
    .then((user) => res.json(user))
    .catch((err) => next(err));
}

function _delete(req, res, next) {
  userService
    ._delete(req.params.id)
    .then((user) => res.json(user))
    .catch((err) => next(err));
}
function getRefugeeProfile(req, res, next) {
  console.log(req.body);
  userService
    .getRefugeeProfile(req.body)
    .then((refugee) => {
      res.json(refugee);
    })
    .catch((err) => next(err));
}
