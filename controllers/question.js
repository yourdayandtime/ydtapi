const express = require("express");
const router = express.Router();
const QuestionService = require("../models/services/question");
router.post("/save", save);
router.delete("delete", _delete);
router.get("/", adminGetAll)

//export module, every file in node is a module
module.exports = router;
//save handles create and update
function save(req, res, next) {
  QuestionService
    .save(req.body)
    .then((question) => res.json(question))
    .catch((error) => next(error));
}

function _delete(req, res, next) {
  QuestionService
    ._delete(req.body.id)
    .then((del) => res.json(del))
    .catch((err) => next(err));
}
function adminGetAll(req, res, next) {
  QuestionService
    .adminGetAll()
    .then((questions) => {
      res.json(questions);
    })
    .catch((err) => next(err));
}
