const express = require("express");
const router = express.Router();
const PageService = require("../models/services/page");
router.post("/create", create);
router.get('/latest',latest);
router.put("/update", update);
router.delete("/delete", _delete);
router.get("/find", find);
router.get("/findByField/:field/:value", findByField);
router.get("/getMenus", findMenus);
//export module, every file in node is a module
module.exports = router;
function findMenus(req, res, next) {
  PageService.getMenus(1)
    .then((menus) => res.json(menus))
    .catch((error) => next(error));
}

function find(req, res, next) {
  PageService.getAll()
    .then((pages) => res.json(pages))
    .catch((error) => next(error));
}

function findByField(req, res, next) {
  PageService.findByField(req.params.field, req.params.value)
    .then((page) => res.json(page))
    .catch((error) => next(error));
}
function create(req, res, next) {
  PageService.create(req.body)
    .then((page) => res.json(page))
    .catch((error) => next(error));
}
function latest(req, res, next) {
  PageService.getLatest()
    .then((page) => res.json(page))
    .catch((error) => next(error));
}
function update(req, res, next) {
  PageService.update(req.body.id, req.body)
    .then((page) => (page ? res.json(page) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function _delete(req, res, next) {
  PageService._delete(req.body.id)
    .then((del) => res.json(del))
    .catch((err) => next(err));
}
