const express = require("express");
const router = express.Router();
const CenterService = require("../models/services/center");
router.post("/create", create);
router.put("/update", update);
router.delete("/delete", _delete);
//export module, every file in node is a module
module.exports = router;
function create(req, res, next) {
  CenterService.create(req.body)
    .then((center) => res.json(center))
    .catch((error) => next(error));
}

function update(req, res, next) {
  CenterService.update(req.body.id, req.body)
    .then((center) => (center ? res.json(center) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function _delete(req, res, next) {
  CenterService._delete(req.body.id)
    .then((del) => res.json(del))
    .catch((err) => next(err));
}