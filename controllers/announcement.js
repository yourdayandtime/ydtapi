const express = require("express");
const router = express.Router();
const AnnService = require("../models/services/announcement");
router.post("/create", create);
router.put("/update", update);
router.delete("delete", _delete);
router.get("/", getAll)

//export module, every file in node is a module
module.exports = router;

function create(req, res, next) {
  AnnService
    .create(req.body)
    .then((ann) => res.json(ann))
    .catch((error) => next(error));
}
function update(req, res, next) {
  AnnService
    .update(req.body.id, req.body)
    .then((ann) => (ann ? res.json(ann) : res.sendStatus(404)))
    .catch((err) => next(err));
}

function _delete(req, res, next) {
  AnnService
    ._delete(req.body.id)
    .then((del) => res.json(del))
    .catch((err) => next(err));
}
function getAll(req, res, next) {
  AnnService
    .getAll()
    .then((anns) => {
      res.json(anns);
    })
    .catch((err) => next(err));
}
